<?php


class UserController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('UserModel');
		$this->load->model('TypeModel');
		$this->load->model('PhoneModel');
		$this->load->library('form_validation');
		$config = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'required'
			),
			array(
				'field' => 'address',
				'label' => 'Address',
				'rules' => 'required'
			),
			array(
				'field' => 'email',
				'label' => 'Email',
				'rules' => 'required|valid_email'
			),
			array(
				'field' => 'password',
				'label' => 'Password',
				'rules' => 'required'
			),
		);
		$this->form_validation->set_rules($config);
	}

	public function home()
	{
		$config['base_url'] = base_url() . 'index.php/UserController/home';
		$config['total_rows'] = $this->PhoneModel->countPhone();
		$config['per_page'] = 3;
		$config['next_link'] = '<img class="mb-1 ml-1 mr-1" src="https://img.icons8.com/ultraviolet/20/000000/circled-chevron-right.png"/>';
		$config['prev_link'] = '<img class="mb-1 ml-1 mr-1" src="https://img.icons8.com/ultraviolet/20/000000/circled-chevron-left.png"/>';
		$this->pagination->initialize($config);
		$start = $this->uri->segment(3);
		$this->load->library('pagination', $config);
		$data['phones'] = $this->PhoneModel->getAll($config['per_page'], $start);
		$data['view'] = 'phone/listPhone';
		$this->load->view("viewMaster", $data);
	}

	public function formLogin()
	{
		$this->load->view('User/formLogin');
	}

	public function login()
	{
		$userLogin = $this->UserModel->checkEmail();
		if ($userLogin) {
			$this->session->set_userdata('user', $userLogin);
			redirect('home');
		} else {
			$this->session->set_flashdata('fail', 'Sai địa chỉ email hoặc mật khẩu');
			redirect('login');
		}
	}

	public function logOut()
	{
		$this->session->sess_destroy();
		redirect('home');
	}

	public function createUser()
	{
		$user = array(
			'name' => $this->input->post('name'),
			'address' => $this->input->post('address'),
			'email' => $this->input->post('email'),
			'password' => $this->input->post('password')
		);
		if ($this->form_validation->run()) {
			$this->UserModel->register($user);
			$this->session->set_flashdata('register_success', 'Đăng ký tài khoản thành công !');
		} else {
			$this->session->set_flashdata("register_fail", 'Đăng ký tài khoản thất bại, hãy nhập đủ các trường');
			redirect('login');
		}
		redirect('login');
	}

	public function information($id)
	{
		$data['phones'] = $this->PhoneModel->getPhonesByIdUser($id);
		$data['user'] = $this->UserModel->getUserById($id);
		$data['view'] = 'User/information';
		$this->load->view('viewMaster', $data);
	}

	public function updateAvatar($id)
	{
		$userById = $this->UserModel->getUserById($id);

		$config['upload_path'] = './avatar';
		$config['allowed_types'] = '*';
		$config['encrypt_name'] = true;
		$this->load->library('upload', $config);
		$this->upload->do_upload('image1');
		$image = $this->upload->data();

		$user = array(
			'name' => $userById->name,
			'address' => $userById->address,
			'email' => $userById->email,
			'password' => $userById->password,
			'avatar' => implode(" ", $image)
		);

		$this->UserModel->updateAvatar($id, $user);

		redirect('information/' . $userById->id);
	}

	public function updateUser($id)
	{
		$user = array(
			'name' => $this->input->post('name'),
			'age' => $this->input->post('age'),
			'gender' => $this->input->post('gender'),
			'address' => $this->input->post('address'),
			'email' => $this->input->post('email')
		);
		$this->UserModel->updateInfoUser($id, $user);

		redirect('information/' . $id);
	}

	public function send_mail_Buy_Phone($id)
	{
		$phone = $this->PhoneModel->getPhoneById($id);
		$from_email = "ledanhquyen1998@gmail.com";
		$to_email = $this->session->userdata('user')->email;

		$this->load->library('email');

		$config['protocol'] = 'smtp';
		$config['smtp_host'] = 'ssl://smtp.googlemail.com';
		$config['smtp_user'] = 'ledanhquyen1998@gmail.com';
		$config['smtp_pass'] = 'Ledanhquyen98';
		$config['smtp_port'] = '465';
		$config['charset'] = 'utf-8';
		$config['newline'] = "\r\n";
		$config['mailtype'] = 'text';

		$this->email->initialize($config);

		$this->email->from($from_email, 'Lê Danh Quyền');
		$this->email->to($to_email);
		$this->email->subject('Đặt Hàng Thành Công');
		$this->email->message('
		Bạn Đã Đặt Mua Sản Phẩm ' . $phone->name . '
		Giá Của Sản Phẩm Là: ' . $phone->price . 'VND' . "
		Cảm Ơn Bạn Đã Đặt Hàng Của Shop !!!
		");

		if ($this->email->send()) {
			$this->session->set_flashdata("success",
				"Đặt Hàng Thành Công, 
				Vui Lòng Kiểm Tra Lại Mail Của Bạn !!!"
			);
		} else {
			$this->session->set_flashdata('fail',
				"Đặt Hàng Thất Bại,
				 Vui Lòng Kiểm Tra Lại"
			);
		}
		redirect('home');
	}
}

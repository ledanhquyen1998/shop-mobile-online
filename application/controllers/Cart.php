<?php


class Cart extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->library('cart');
		$this->load->model('PhoneModel');
		$this->load->model('TypeModel');
	}

	public function showCart()
	{
		if (!$this->session->userdata('user')) {
			redirect('login');
		}
		$data['types'] = $this->TypeModel->getAll();
		$data['products'] = $this->cart->contents();
		$data['view'] = 'cart/listCart';
		$this->load->view('viewMaster', $data);
	}

	public function addToCart($id)
	{
		if (!$this->session->userdata('user')) {
			redirect('login');
		}
		$data = $this->PhoneModel->getPhoneById($id);
		$phone = array(
			'name' => $data->name,
			'image' => $data->avatar,
			'id' => $data->id,
			'price' => $data->price,
			'qty' => 1,
			'user' => $data->user_id
		);
		if ($this->cart->insert($phone)) {
			$this->session->set_flashdata('success', 'Thêm vào giỏ thành công');
		} else {
			$this->session->set_flashdata('fail', 'Thêm vào giỏ thất bại');
		}
		redirect('PhoneController/phoneDetails/' . $data->id);
//		redirect($this->uri->uri_string());
	}

	public function deleteProduct($rowid)
	{
		$this->cart->update(array('rowid' => $rowid, 'qty' => 0));
		redirect('your-cart');
	}

	public function deleteAllProduct()
	{
		$this->cart->destroy();
		$this->session->set_flashdata('done', 'Đã Xóa Hết Trong Giỏ Hàng !!');
		redirect('your-cart');
	}
}

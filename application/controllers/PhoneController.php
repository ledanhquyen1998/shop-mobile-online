<?php


class PhoneController extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
		$this->load->model('PhoneModel');
		$this->load->model('TypeModel');
		$this->load->helper('form');
		$this->load->library('form_validation');
		$config = array(
			array(
				'field' => 'name',
				'label' => 'Name',
				'rules' => 'required',
			),
			array(
				'field' => 'color',
				'label' => 'Color',
				'rules' => 'required',
			),
			array(
				'field' => 'price',
				'label' => 'Price',
				'rules' => 'required',
			),
			array(
				'field' => 'cpacity',
				'label' => 'Cpacity',
				'rules' => 'required',
			)
		);
		$this->form_validation->set_rules($config);
	}

	public function formAdd()
	{
		if (!$this->session->userdata('user')) {
			redirect('login');
		}
		$data['types'] = $this->TypeModel->getAll();
		$this->load->view('phone/formAdd', $data);
	}

	public function create()
	{
		$config['upload_path'] = './upload';
		$config['allowed_types'] = '*';
		$config['encrypt_name'] = true;
		$this->load->library('upload', $config);
		$this->upload->do_upload('image');
		$image = $this->upload->data();

		$phone = array(
			'name' => $this->input->post('name'),
			'type_id' => $this->input->post('type_id'),
			'user_id' => $this->input->post('user_id'),
			'color' => $this->input->post('color'),
			'price' => $this->input->post('price'),
			'cpacity' => $this->input->post('cpacity'),
			'avatar' => implode(" ", $image)
		);
		if (empty($_FILES['image']['name'])) {
			$this->form_validation->set_rules('image', 'Image', 'required');
		}
		if ($this->form_validation->run()) {
			$this->PhoneModel->add($phone);
		} else {
			$data['types'] = $this->TypeModel->getAll();
			return $this->load->view('phone/formAdd', $data);
		}

		redirect('home');
	}

	public function phoneDetails($id)
	{
		$data['types'] = $this->TypeModel->getAll();
		$data['phone'] = $this->PhoneModel->getPhoneById($id);
		$data['view'] = 'phone/phoneDetails';
		return $this->load->view('viewMaster', $data);
	}

	public function search()
	{
		$name = $this->input->post('search');
		$type = $this->input->post('type');
		$cpacity = $this->input->post('cpacity');
		$data['search'] = $this->input->post('search');
		$data['phones'] = $this->PhoneModel->searchForKeyword($name, $type, $cpacity);
		$data['types'] = $this->TypeModel->getAll();
		$data['view'] = 'phone/search';
		$this->load->view('viewMaster', $data);
	}

	public function edit($id)
	{
		$data['types'] = $this->TypeModel->getAll();
		$data['phone'] = $this->PhoneModel->getPhoneById($id);
		$this->load->view('phone/edit', $data);
	}

	public function deletePhone($id, $user_id)
	{
		$this->PhoneModel->delete($id);
		redirect('information/' . $user_id);
	}

	public function updatePhone($id, $idUser)
	{

		$phoneUpdate = array(
			'name' => $this->input->post('name'),
			'type_id' => $this->input->post('type_id'),
			'user_id' => $this->input->post('user_id'),
			'color' => $this->input->post('color'),
			'price' => $this->input->post('price'),
			'cpacity' => $this->input->post('cpacity'),
		);

		if ($this->form_validation->run()) {
			$this->PhoneModel->update($id, $phoneUpdate);
		} else {
			$data['types'] = $this->TypeModel->getAll();
			$data['phone'] = $this->PhoneModel->getPhoneById($id);
			return $this->load->view('phone/edit', $data);
		}

		redirect('information/' . $idUser);
	}

	public function googleMap()
	{
			$this->load->view('maps/maps');
	}
}

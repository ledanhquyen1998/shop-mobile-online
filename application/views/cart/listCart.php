<h1 id="title" style="text-align: center; font-family: 'URW Gothic L'; font-weight: bold;color: orange;"
	class="mt-2 mb-2">Giỏ Hàng Của Bạn</h1>
<h3 id="title" style="text-align: center;color: red">______________________________________</h3>
<div style="text-align: center">
	<a href="<?php echo site_url('deleteAll') ?>" class="btn btn-danger mb-2"
	   onclick="return confirm('Bạn Muốn Xóa Hết Sản Phẩm Trong Giỏ Hàng Thật Chứ ?')">Xóa Hết</a>
</div>
<div style="text-align: center; color: blue">
	<?php echo $this->session->flashdata('done') ?>
</div>
<?php if (!$products) { ?>
	<div style="text-align: center; font-size: 25px">
		Giỏ Hàng Của Bạn Không Có Gì !!!
	</div>
<?php } ?>
<div class="container">
	<div class="card" style="border: 5px">
		<div class="card-body ml-3">
			<?php foreach ($products as $key => $product): ?>
					<div class="card mt-3 ml-4" style="width: 30rem; float: left">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-4">
								<img src="<?php echo base_url() ?>upload/<?php echo $product['image'] ?>"
									 style="height: 150px">
							</div>
							<div class="col-lg-7 offset-1">
								<div id="cart" class="mt-2" style="font-size: 25px; font-weight: bold; color: #56baed">
									<a href="<?php echo site_url('phoneDetails') ?>/<?php echo $product['id'] ?>"><?php echo $product['name'] ?></a>
								</div>
								<div>
									<div class="row">
										<div class="col-lg-5 mt-1">
											<lable>Số Lượng:</lable>
										</div>
										<div class="col-lg-6">
											<table style="width: 100px">
												<tr>
													<th>
														<button class="minus"><img src="https://img.icons8.com/nolan/20/minus-math.png"/></button>
													</th>
													<th><input id="qty" type="text" value="<?php echo $product['qty'] ?>"
															   style="width: 30px; text-align: center; border: white"></th>
													<th>
														<button class="total"><img src="https://img.icons8.com/nolan/20/plus-math.png"/>
														</button></th>
												</tr>
											</table>
										</div>
									</div>

								</div>
								<div class="mt-2">
									Giá: <label style="font-size: 20px"><?php echo $product['price'] ?> (VNĐ)</label>
								</div>
								<div>
									<a href="<?php echo site_url('delete') ?>/<?php echo $product['rowid'] ?>"
									   style="color: white" class="btn btn-dark"
									   onclick="return confirm('Bạn Muốn Xóa Sản Phẩm Này Khỏi Giỏ Hàng Của Bạn Chứ ?')">Xóa
										Khỏi Giỏ Hàng
										<img src="https://img.icons8.com/color/30/000000/minus.png"/>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
		<div style="text-align: center; font-size: 20px">
			Tổng số tiền trong giỏ: <label style="font-size: 30px"><?php echo $this->cart->total() ?> (VNĐ)</label>
		</div>
	</div>
</div>


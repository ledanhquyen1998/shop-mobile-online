<h1 id="title" style="text-align: center; font-family: 'URW Gothic L'; font-weight: bold;color: orange;"
	class="mt-2 mb-2">Trang Cá Nhân</h1>
<h3 id="title" style="text-align: center;color: red">______________________________________</h3>

<div class="container mt-5">
	<div class="card">
		<div>
			<div>
				<label style="background: gray; color: white" class="card-header user" id="information">Thông Tin Của Bạn</label>
				<label class="card-header user" id="product">Sản Phẩm Đã Đăng</label>
				<label class="card-header user" id="ordered">Đã Đặt Mua</label>
			</div>
		</div>
		<form action="" method="post" enctype="multipart/form-data">
			<div class="card-body information">
				<div class="card">
					<div class="card-body">
						<div class="row">
							<div class="col-lg-5">
								<div style="position: relative">
									<form action="<?php echo site_url('updateAvatar') ?>/<?php echo $user->id ?>"
										  method="post" enctype="multipart/form-data">
										<img style="width: 100%"
											 src="<?php echo base_url() ?>avatar/<?php echo $user->avatar ?>"
											 alt="">
										<div style="position: absolute; top: 90%; left: 25%;">
											<div>
												<label class="btn btn-outline-secondary button-update-image-user">Cập
													Nhật Ảnh
													Đại Diện
												</label>
												<button class="btn btn-primary save mb-2" type="submit"
														style="display: none">Lưu
												</button>
											</div>
											<input class="update-image-user" name="image1" id="image1" type="file"
												   style="display: none">
										</div>
									</form>
								</div>
							</div>

							<div class="col-lg-7">
								<div class="info-user">
									<div class="row">
										<h6 class="mt-2">Name:</h6>
										<h3 style="margin-left: 10%"
											class="show-info-user"><?php echo $user->name ?></h3>
									</div>
									<div class="row">
										<h6 class="mt-4">Age:</h6>
										<h3 style="margin-left: 15%"
											class="mt-2 show-info-user"><?php echo $user->age ?></h3>
									</div>
									<div class="row">
										<h6 class="mt-4">Gender:</h6>
										<h3 style="margin-left: 10%"
											class="mt-2 show-info-user"><?php echo $user->gender ?></h3>
									</div>
									<div class="row">
										<h6 class="mt-4">Address:</h6>
										<h3 class="mt-3 show-info-user"
											style="margin-left: 10%"><?php echo $user->address ?></h3>
									</div>
									<div class="row">
										<h6 class="mt-4">Email:</h6>
										<h3 class="mt-3 show-info-user"
											style="margin-left: 10%; "><?php echo $user->email ?></h3>
									</div>
									<div style="text-align: center">
										<label class="btn btn-warning mt-4 button-edit-user">Sửa Thông Tin</label>
									</div>
								</div>

								<div class="edit-info-user mt-5" style="display: none">
									<form action="<?php echo site_url('updateInfoUser') ?>/<?php echo $user->id ?>"
										  method="post">
										<div class="row">
											<div class="col-lg-3">
												<div>
													<h6 style="margin-left: 10%" class="mt-4">Tên:</h6>
												</div>
												<div>
													<h6 style="margin-left: 10%" class="mt-4">Tuổi:</h6>
												</div>
												<div>
													<h6 style="margin-left: 10%" class="mt-4">Giới Tính:</h6>
												</div>
												<div>
													<h6 style="margin-left: 10%" class="mt-4">Địa Chỉ:</h6>
												</div>
												<div>
													<h6 style="margin-left: 10%" class="mt-4">Email:</h6>
												</div>
											</div>
											<div class="col-lg-9">
												<div class="mt-2">
													<input class="form-control input-edit" type="text" name="name"
														   value="<?php echo $user->name ?>">
												</div>
												<div class="mt-2">
													<input class="form-control input-edit" type="number" name="age"
														   value="<?php echo $user->age ?>">
												</div>
												<div class="mt-2">
													<select class="form-control select-edit" name="gender" id="gender">
														<option value="">Giới Tính</option>
														<option <?php if ($user->gender == "Nữ") { ?> selected value="Nữ"
														<?php } ?>>Nữ
														</option>
														<option <?php if ($user->gender == "Nam") { ?> selected
															value="Nam" <?php } ?>>Nam
														</option>
														<option <?php if ($user->gender == "Khác") { ?> selected
															value="Khác" <?php } ?> >Khác
														</option>
													</select>
												</div>
												<div class="mt-2">
													<input class="form-control input-edit" type="text" name="address"
														   value="<?php echo $user->address ?>">
												</div>
												<div class="mt-2">
													<input class="form-control input-edit" type="text" name="email"
														   value="<?php echo $user->email ?>" readonly>
												</div>
												<div class="save-edit-user">
													<button class="btn btn-primary mt-3" type="submit">Lưu</button>
													<label class="btn btn-dark mt-4 not-edit">Hủy</label>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>

						</div>

					</div>
				</div>
			</div>
		</form>

		<div class="card-body product" style="display: none">
			<div class="row ml-3">
				<?php foreach ($phones as $key => $phone): ?>

					<div class="card ml-4 mt-2" style="width: 45%">
						<div class="card-body">
							<div class="row">
								<div class="col-lg-4">
									<img style="width: 100%"
										 src="<?php echo base_url() ?>upload/<?php echo $phone->avatar ?>" alt="">
								</div>
								<div id="cart" class="col-lg-8">
									<h3>
										<a href="<?php echo site_url('phoneDetails') ?>/<?php echo $phone->id ?>"><?php echo $phone->name ?></a>
									</h3>
									<h6>Giá: <?php echo $phone->price ?> (VND)</h6>
									<h6>Màu: <?php echo $phone->color ?> </h6>
									<h6>Dung Lượng: <?php echo $phone->cpacity ?></h6>

									<div>
										<a href="<?php echo site_url('editPhone') ?>/<?php echo $phone->id ?>"
										   class="btn btn-outline-success">Sửa</a>
										<a href="<?php echo site_url('deletePhone') ?>/<?php echo $phone->id ?>/<?php echo $this->session->userdata('user')->id ?>"
										   onclick="return confirm('Bạn chắc chắn với thao tác này chứ ?')"
										   class="btn btn-outline-danger button-delete">Xóa</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php endforeach; ?>
			</div>
		</div>

	</div>
</div>

<!doctype html>
<html lang="en">
<head>
	<meta c style="font-size: 17px; font-weight: bold" harset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Document</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
		  integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('css/login.css')?>">
</head>
<style>
	body{
		background-image: url("<?php echo base_url('banner/login.png')?>");
		background-size: auto;
	}
</style>
<body>
<div class="wrapper fadeInDown">
	<div id="formContent">
		<!-- Tabs Titles -->
		<label style="color: blue" id="login">Đăng Nhập</label>
		<label id="register">Đăng Ký</label>

		<!-- Icon -->
		<div class="fadeIn first">
			<img src="https://img.icons8.com/officel/40/000000/beach-soccer.png"/>
		</div>

		<!-- Login Form -->
		<div class="login">
			<form action="<?php echo site_url('UserController/login') ?>" method="post">
				<div style="color: #2cc36b"><?php echo $this->session->flashdata('register_success')?></div>
				<div style="color: red"><?php echo $this->session->flashdata('register_fail')?></div>
				<div style="color: red"><?php echo $this->session->flashdata('fail')?></div>
				<input type="text" id="login" class="fadeIn second" name="email" placeholder="Email">
				<input style="width: 330px" type="password" id="password" class="fadeIn third" name="password"
					   placeholder="Password">
				<img class="eye2" style="display: none"
					 src="https://img.icons8.com/ios-glyphs/30/000000/invisible.png"/>
				<img class="eye1" src="https://img.icons8.com/ios-glyphs/30/000000/visible.png"/>

				<input style="margin-top: 10px" type="submit" class="fadeIn fourth" value="Log In">
			</form>
		</div>

		<div class="register" style="display: none">
			<form action="<?php echo site_url('register')?>" method="post">
				<div style="font-size: 17px; font-weight: bold">
					Tên
				</div>
				<input type="text" id="name" class="fadeIn second" name="name" placeholder="Tên Của Bạn">
				<div style="color: red; font-weight: bold; font-size: 13px; text-align: center; font-family: FontAwesome"><?php echo form_error('name') ?></div>

				<div style="font-size: 17px; font-weight: bold">
					Địa Chỉ
				</div>
				<input type="text" id="address" class="fadeIn third" name="address" placeholder="Địa Chỉ Của Bạn">
				<div style="font-size: 17px; font-weight: bold">Địa Chỉ Email</div>
				<input type="text" id="login" class="fadeIn second" name="email" placeholder="Địa Chỉ Email Của Bạn">
				<div style="font-size: 17px; font-weight: bold">
					Mật Khẩu
				</div>
				<input type="text" id="password" class="fadeIn third" name="password" placeholder="Mật Khẩu Của Bạn">
				<div style="font-size: 17px; font-weight: bold">
					Nhập Lại Mật Khẩu
				</div>
				<input type="text" id="password" class="fadeIn third" name="re-password" placeholder="Mật Khẩu Của Bạn">
				<button type="submit" class="btn btn-info mt-2 mb-2">Đăng Ký</button>
				<a href="<?php echo site_url('home') ?>" class="btn btn-dark mt-2 mb-2">Hủy</a>
			</form>
		</div>
		<!-- Remind Passowrd -->
		<div class="footer" id="formFooter">
			<a class="underlineHover" href="#">Forgot Password?</a>
		</div>

	</div>
</div>
</body>
</html>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
<script src="<?php echo base_url() ?>js/login.js"></script>
<script>
	$(document).ready(function () {
		$('#register').click(function () {
			$('#formContent').max_width = 600 + 'px'
		})
	})
</script>

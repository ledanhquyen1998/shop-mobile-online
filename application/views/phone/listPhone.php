<div class="container">
	<h1 id="title" style="text-align: center; font-family: 'URW Gothic L'; font-weight: bold;color: orange;"
		class="mt-2 mb-2">Danh
		Sách Sản Phẩm</h1>
	<h6 id="title" style="text-align: center">Hãy cùng khám phá các dòng Iphone hot nhất, chuẩn nhất, đẹp mắt nhất và
		lựa chọn sử
		dụng cho mình một sản phẩm ưng ý nhất</h6>
	<h3 id="title" style="text-align: center;color: red">______________________________________</h3>
	<div style="color: mediumspringgreen; font-size: 20px; text-align: center"><?php echo $this->session->flashdata('success'); ?></div>
	<div style="color: red; font-size: 20px; text-align: center"><?php echo $this->session->flashdata('fail'); ?></div>
	<div class="row mt-5">
		<div class="row ml-3">
			<?php foreach ($phones as $key => $phone): ?>
				<div class="col-lg-4 mt-4">
					<div class="item-product">
						<div class="item-image">
							<img style="max-width: 100%"
								 src="<?php echo base_url() ?>upload/<?php echo $phone['avatar'] ?>">
							<div class="overlay"></div>
							<div class="details">
								<div>
									<a class="q btn btn-success" style="border-radius: 20px"
									   href="<?php echo site_url('phoneDetails') ?>/<?php echo $phone['id'] ?>">Chi
										Tiết</a>
								</div>
								<div class="mt-3" style="text-align: center">
									<a class="q btn btn-warning" style="border-radius: 20px"
									   href="<?php echo site_url('Cart/addToCart') ?>/<?php echo $phone['id'] ?>">Thêm
										Vào Giỏ +</a>
								</div>
								<div class="mt-3" style="text-align: center">
									<a class="btn btn-info" style="border-radius: 20px"
									   href="<?php echo site_url('buyPhoneSendEmail') ?>/<?php echo $phone['id'] ?>">Đặt
										Mua</a>
								</div>
							</div>
						</div>
						<div id="cart" class="card-body">
							<h3 style="font-family: FontAwesome; font-weight: bold; text-align: center">
								<a href="<?php echo site_url('phoneDetails') ?>/<?php echo $phone['id'] ?>"><?php echo $phone['name'] ?>
								</a>
							</h3>
						</div>
					</div>
				</div>
			<?php endforeach; ?>
		</div>
	</div>
	<div class="row mt-2" style="margin-left: 50%; font-size: 20px">
		<?php echo $this->pagination->create_links(); ?>
	</div>
</div>


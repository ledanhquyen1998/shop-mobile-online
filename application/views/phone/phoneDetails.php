<h1 style="font-family: mry_KacstQurn; font-weight: bold; color: orange; text-align: center" class="mt-3">Chi Tiết Sản
	Phẩm</h1>
<h3 id="title" style="text-align: center;color: red">________________________________________</h3>
<div class="container">
	<div class="row ml-5">
		<div class="col-lg-5 offset-1">
			<img src="<?php echo base_url() ?>/upload/<?php echo $phone->avatar ?>" style="width: 100%">
		</div>
		<div class="col-lg-5 mt-5">
			<div>
				Sản Phẩm: <label
						style="font-size: 25px; font-weight: bold; font-family: mry_KacstQurn; color: #56baed"><?php echo $phone->name ?></label>
			</div>
			<div class="mt-3">
				Màu: <label
						style="font-size: 25px; font-weight: bold; font-family: mry_KacstQurn; color: #56baed"><?php echo $phone->color ?></label>
			</div>
			<div class="mt-3">
				Dung Lượng: <label
						style="font-size: 25px; font-weight: bold; font-family: mry_KacstQurn; color: #56baed"><?php echo $phone->cpacity ?></label>
			</div>
			<div class="mt-3">
				Loại: <label
						style="font-size: 25px; font-weight: bold; font-family: mry_KacstQurn; color: #56baed"><?php echo $phone->type ?></label>
			</div>
			<div class="mt-3">
				Giá Tiền: <label
						style="font-size: 25px; font-weight: bold; font-family: mry_KacstQurn; color: #56baed"><?php echo $phone->price ?></label>
			</div>
			<div class="mt-3">
				<a style="height: 44px" class="btn btn-dark" href="<?php echo site_url('home') ?>">Quay Lại</a>
				<a href="<?php echo site_url('Cart/addToCart') ?>/<?php echo $phone->id ?>" class="btn btn-warning">Thêm
					Vào Giỏ <img src="https://img.icons8.com/color/30/000000/add.png"/>
				</a>
			</div>
			<?php if ($this->session->flashdata('success')) { ?>
				<div class="mt-2 ml-2" style="font-size: 20px; color: springgreen; font-weight: bold">
					<?php echo $this->session->flashdata('success') ?>
				</div>
			<?php } else { ?>
				<div class="ml-2" style="font-size: 20px; color: red; font-weight: bold">
					<?php echo $this->session->flashdata('fail') ?>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

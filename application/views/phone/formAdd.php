<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Form Add</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
		  integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('css/formAdd.css')?>">
</head>
<body style="background-image: url('<?php echo base_url('banner/away1.jpg') ?>'); background-size: cover">
<div>
	<div class="card" style="width: 45%; background: white; border-radius: 10px">
		<div class="card-body">
			<div class="row">
				<div class="col-lg-5" style="text-align: center">
					<img class="ml-5 mt-3" src="<?php echo base_url('banner/iphone11.jpg') ?>" style="width: 100%; height: 90%">
				</div>
				<div class="col-lg-6 offset-1">
					<form action="<?php echo site_url('create_success') ?>" method="post" enctype="multipart/form-data">
						<div class="title">
							Thêm Mới Sản Phẩm
						</div>
						<input class="ml-3 mt-3"
							   type="text" name="name" placeholder="Tên Sản Phẩm">
						<div class="errors"><?php echo form_error('name') ?></div>
						<input class="ml-3 mt-3" type="text" name="user_id" value="<?php echo $this->session->userdata('user')->id ?>"
							   style="display: none">
						<input class="ml-3 mt-3"
							   type="text" name="color" placeholder="Màu Sản Phẩm">
						<div class="errors"><?php echo form_error('color') ?></div>


							<div class="col-lg-5 mt-3 cpacity">
								<select style="width: 148%" class="form-control select-form-add" name="cpacity" id="">
									<option value="">Dung Lượng</option>
									<option value="16GB">16GB</option>
									<option value="32GB">32GB</option>
									<option value="64GB">64GB</option>
									<option value="128GB">128GB</option>
								</select>
							</div>
							<div class="col-lg-5 mt-3 type">
								<select class="form-control select-form-add" name="type_id" id="">
									<option value="">Loại</option>
									<?php foreach ($types as $key => $type) : ?>
										<option value="<?php echo $type->id ?>"><?php echo $type->type ?></option>
									<?php endforeach; ?>
								</select>
							</div>

						<div class="errors"><?php echo form_error('cpacity') ?></div>
						<input class="ml-3"
							   type="number" name="price" placeholder="Giá Sản Phẩm(VND)">
						<div class="errors"><?php echo form_error('price') ?></div>

						<input class="ml-3" name="image" id="image" type="file" style="margin-top: 10px">
						<div class="errors"><?php echo form_error('image') ?></div>

						<div style="margin-top: 20px; margin-left: 20%">
							<a style="color: white" href="<?php echo site_url('home') ?>" class="btn btn-dark">Hủy</a>
							<button type="submit" value="save" class="btn btn-primary">Thêm</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>

<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport"
		  content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Edit</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
		  integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('css/edit.css')?>">
</head>
<body style="background-image: url('<?php echo base_url('banner/edit.jpg') ?>'); background-size: cover">
<div class="card" style="background: white; width: 40%; border-radius: 10px">
	<div class="row">
		<div class="col-lg-5">
			<img class="mt-3 ml-2" src="<?php echo base_url() ?>upload/<?php echo $phone->avatar ?>"
				 style="width: 100%; height: 90%" alt="">
		</div>
		<div class="col-lg-7">
			<form method="post"
				  action="<?php echo site_url('updatePhone') ?>/<?php echo $phone->id ?>/<?php echo $phone->user_id ?>">
				<div class="mt-3"
					 style="font-family: 'mry_KacstQurn'; font-size: 25px; font-weight: bold; color: orange">
					Sửa Thông Tin Sản Phẩm
				</div>
				<div class="row">
					<div class="col-lg-5">
						<div class="mt-4 title">Tên:</div>
						<div class="mt-3 title">Màu:</div>
						<div class="mt-4 title">Loại:</div>
						<div class="mt-4 title">Dung Lượng:</div>
						<div class="mt-4 title">Giá:</div>
					</div>
					<div class="col-lg-7">
						<div>
							<input type="text" name="user_id" value="<?php echo $phone->user_id ?>" style="display: none">
						</div>
						<div>
							<input class="mt-3" type="text" value="<?php echo $phone->name ?>" name="name" placeholder="Tên Sản Phẩm">
							<div class="errors"><?php echo form_error('name') ?></div>
						</div>
						<div>
							<input class="mt-3" type="text" name="color" value="<?php echo $phone->color ?>" placeholder="Màu Sản Phẩm">
							<div class="errors"><?php echo form_error('color') ?></div>
						</div>
						<div>
							<select class="form-control select mt-3" style="width: 80%" name="type_id" id="">
								<option value="">Loại</option>
								<?php foreach ($types as $key => $type) : ?>
									<option <?php if ($type->type == $phone->type) { ?> selected value="<?php echo $type->id ?>" <?php } ?>><?php echo $type->type ?></option>
								<?php endforeach; ?>
							</select>

						</div>
						<div>
							<select style="width: 80%" class="form-control select mt-3" name="cpacity" id="">
								<option value="">Dung Lượng</option>
								<option <?php if ($phone->cpacity == "16GB") { ?> selected value="16GB" <?php } ?>>
									16GB
								</option>
								<option <?php if ($phone->cpacity == "32GB") { ?> selected value="32GB" <?php } ?>>
									32GB
								</option>
								<option <?php if ($phone->cpacity == "64GB") { ?> selected value="64GB" <?php } ?>>
									64GB
								</option>
								<option <?php if ($phone->cpacity == "128GB") { ?> selected value="128GB" <?php } ?>>
									128GB
								</option>
							</select>
						</div>
						<div>
							<input class="mt-3" type="text" name="price" value="<?php echo $phone->price ?>" placeholder="Màu Sản Phẩm">(VND)
							<div class="errors"><?php echo form_error('price') ?></div>
						</div>
					</div>
				</div>
				<div class="mt-4 mb-2" style="text-align: center">
					<a href="<?php echo site_url('information') ?>/<?php echo $phone->user_id ?>"
					class="btn btn-dark">Hủy</a>
					<button type="submit" class="btn btn-primary mr-2">Lưu</button>
				</div>
			</form>
		</div>
	</div>
</div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>

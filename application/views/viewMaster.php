<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Welcome To Shop Mobile Online</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
		  integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
	<link rel="stylesheet" href="<?php echo base_url('css/viewMaster.css') ?>" type="text/css">
</head>
<body>
<div class="header" style="background: lightsteelblue">
	<div class="row">
		<div class="col-lg-6" style="text-align: center; float: right">
			<div id="header">
				<a href="<?php echo site_url('home') ?>"><img src="https://img.icons8.com/officel/30/000000/home.png"/></a>
				<a class="ml-3" href="">Tiện Ích</a>
				<a class="ml-3" href="">Chăm Sóc Khác Hàng</a>
				<a class="ml-3" href="">Sale</a>
				<a class="ml-3" href="">Khuyến Mãi</a>
			</div>
		</div>
		<div class="col-lg-6">
			<div id="header">
				<a href="https://www.facebook.com/LDQ.BigBoss.10" class="mr-3">Facebook</a>
				<a href="https://mail.google.com/mail/u/0/#inbox" class="mr-3">Email</a>
				<a href="<?php echo site_url('PhoneController/googleMap') ?>" class="mr-3">Maps</a>
				<?php if ($this->session->userdata('user')) { ?>
					<?php if ($this->session->userdata('user')) {
						echo "<a class='dropdown-toggle' style='font-size: 20px; color: hotpink' href='' id='navbarDropdown' role='button' data-toggle='dropdown'
				   aria-haspopup='true' aria-expanded='false'>
						 {$this->session->userdata('user')->name}
					</a>";
					} else {
						echo "<a class='ml-2' href='http://localhost/webnews/index.php/UserController/formLogin'>Login/Register</a>";
					} ?>
					<?php if ($this->session->userdata('user')) { ?>
						<div class="dropdown-menu" aria-labelledby="navbarDropdown"
							 style="width: 200px; margin-left: -6%; background: white">
							<a style="text-align: center; font-size: 20px; color:deepskyblue;" class="dropdown-item"
							   href="<?php echo site_url('information') ?>/<?php echo $this->session->userdata('user')->id ?>">
								Trang Cá Nhân
							</a>
							<a style="text-align: center; font-size: 20px; color:deepskyblue;" class="dropdown-item"
							   href="<?php echo site_url('logout') ?>">Đăng Xuất</a>
						</div>
					<?php } ?>
				<?php } else { ?>
					<?php echo "<a style='font-size: 20px' href='http://localhost/shopmobile/index.php/login'>Đăng Nhập</a>" ?>
				<?php } ?>
			</div>
		</div>
	</div>
	<div id="image" style="position: absolute">
		<img src="<?php echo base_url() ?>banner/banner.png" alt="" style="height: 400px; width: 100%">
	</div>
	<div id="search" style="position: relative">
		<form method="post" action="<?php echo site_url('search') ?>" class="form-inline my-2 my-lg-0">
			<input value="<?php if (!empty($search)) {
				echo $search ?><?php } ?>" name="search" style="width: 250px; border-radius: 20px"
				   class="form-control mr-sm-2"
				   type="search"
				   placeholder="Nhập Dữ Liệu Cần Tìm"
				   aria-label="Search">
			<select style="border-radius: 20px" class="form-control" name="type" id="type">
				<option style="border-radius: 20px" value="">Thể Loại</option>
				<?php foreach ($types as $key => $type): ?>
					<option style="border-radius: 20px"
							value="<?php echo $type->id ?>"><?php echo $type->type ?></option>
				<?php endforeach; ?>
			</select>
			<select style="margin-left: 7px;border-radius: 20px" class="form-control" name="cpacity" id="">
				<option style="text-align: center" value="">Dung Lượng</option>
				<option style="text-align: center" value="16GB">16GB</option>
				<option style="text-align: center" value="32GB">32GB</option>
				<option style="text-align: center" value="64GB">64GB</option>
				<option style="text-align: center" value="128GB">128GB</option>
			</select>
			<button style="margin-left: 7px; border-radius: 20px" class=" my-2 my-sm-0" type="submit">
				<img src="https://img.icons8.com/ios/35/000000/search.png"/>
			</button>
		</form>
	</div>

</div>
<div id="cart" class="cart1">
	<a id="upload" href="<?php echo site_url('create') ?>">Tải Lên  <img
				src="https://img.icons8.com/ios/50/000000/upload-to-cloud.png"/></a>
	<a style="margin-left: 30px" href="<?php echo site_url('your-cart') ?>">Giỏ Hàng <img
				src="https://img.icons8.com/dotty/50/000000/shopping-cart.png"/></a>
</div>

<div class="content">
	<?php $this->load->view($view); ?>
</div>
<div class="footer">

</div>
</body>
</html>
<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
		integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n"
		crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
		integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo"
		crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
		integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6"
		crossorigin="anonymous"></script>
<script src="<?php echo base_url() ?>js/viewMaster.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-
         Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous">

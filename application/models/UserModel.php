<?php


class UserModel extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function getAll()
	{
		return $this->db->get('users')->result();
	}

	public function checkEmail()
	{
		$userLogin['email'] = $this->input->post('email');
		$userLogin['password'] = $this->input->post('password');
		return $this->db->get_where('users', $userLogin)->row();
	}

	public function register($user)
	{
		return $this->db->insert('users', $user);
	}

	public function getUserById($id)
	{
		return $this->db->query("SELECT * FROM users WHERE id=" . $id)->row();
	}

	public function updateAvatar($id, $user)
	{

		$this->db->where('id', $id);
		return $this->db->update('users', $user);
	}

	public function updateInfoUser($id, $user)
	{
		$this->db->where('id', $id);
		return $this->db->update('users', $user);
	}

	public function getPhonesByIdUser($id)
	{
		return $this->db->query("SELECT * FROM phones WHERE phones.user_id =" . $id)->result();
	}

	public function getAllUser()
	{
		return $this->db->get('users')->result();
	}
}

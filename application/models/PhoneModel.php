<?php


class PhoneModel extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function getAll($total, $start)
	{
		$this->db->limit($total, $start);
		$query=$this->db->get("phones");
		return $query->result_array();
	}

	public function countPhone()
	{
		return $this->db->select()->get('phones')->num_rows();
	}

	public function add($phones)
	{
		return $this->db->insert('phones', $phones);
	}

	public function getPhoneById($id)
	{
		return $this->db->query("SELECT phones.id,phones.name,phones.color,phones.avatar,phones.user_id,phones.price,types.type,phones.cpacity
								 FROM phones
								 INNER JOIN types
								 ON phones.type_id = types.id
								 WHERE phones.id =" . $id)->row();
	}

	public function searchForKeyword($name, $type, $cpacity)
	{
		return $this->db->query("SELECT phones.id,phones.name,phones.color,phones.price,phones.cpacity,phones.avatar,phones.user_id,types.type FROM phones
								 INNER JOIN types 
								 ON phones.type_id = types.id 
								 WHERE phones.name LIKE '%$name%'
								 AND (phones.type_id LIKE '%$type%')
								 AND (phones.cpacity LIKE '%$cpacity%')")->result();
	}

	public function getPhonesByIdUser($id)
	{
		return $this->db->query("SELECT * FROM phones WHERE phones.user_id =" . $id)->result();
	}

	public function delete($id)
	{
		$this->db->where('id', $id);
		$this->db->delete('phones');
	}

	public function update($id, $phoneUpdate)
	{
		$this->db->where("id", $id);
		$this->db->update("phones", $phoneUpdate);
	}
}
